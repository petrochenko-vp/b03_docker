FROM python:3.11-slim

WORKDIR /app

COPY pdm.lock pyproject.toml ./

RUN pip install pdm

RUN pdm install

ENTRYPOINT [ "/bin/bash" ]
